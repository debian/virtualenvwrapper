virtualenvwrapper (4.8.4-4) unstable; urgency=medium

  * Fix "virtualenvwrapper incorrectly dropped virtualenv dependency"
    by adding it back to the virtualenvwrapper binary package (Closes:
    #939299)
  * Fix "no man pages" by applying the patch by Diane Trout and updating the
    patch content for the current package version (Closes: #615267)
  * command 'allvirtualenvs' is provided by upstream release 4.8.4 (Closes:
    #932923)
  * Fix "bash completion lacks testing for availability" by adding
    availability tests for the virtualenvwrapper scripts (Closes: #857374)

 -- Jan Dittberner <jandd@debian.org>  Tue, 03 Sep 2019 18:09:47 +0200

virtualenvwrapper (4.8.4-3) unstable; urgency=medium

  * Re-upload to unstable

 -- Jan Dittberner <jandd@debian.org>  Sat, 31 Aug 2019 14:17:10 +0200

virtualenvwrapper (4.8.4-2) experimental; urgency=medium

  * add Breaks/Replaces virtualenvwrapper (<< 4.8) to virtualenvwrapper-doc
    and python3-virtualenvwrapper (Closes: #936069)
  * upload to unstable without Python2 support (Closes: #938786)

 -- Jan Dittberner <jandd@debian.org>  Sat, 31 Aug 2019 13:55:48 +0200

virtualenvwrapper (4.8.4-1) experimental; urgency=medium

  [ Joel Cross ]
  * Add Python 3 support by splitting into multiple packages (Closes: #760080)
  * Bump Debhelper and standards version
  * Build using pybuild
  * Update Vcs-Git/Vcs-Browser fields in d/control
  * Patches: Correctly decode bytestring in fix-python3-sphinx-build.patch
  * Patches: Actually remove shebang in
    remove-virtualenvwrapper_lazy-shebang.patch

  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

  [ Nicholas D Steeves ]
  * d/control: Drop python-virtualenvwrapper, the Python 2 variant of this
    package which depends on cruft package python-stevedore (Closes: #933822)
  * d/rules: Drop operations on non-existent python-virtualenvwrapper.
  * d/control: Drop python2 dependencies.
  * d/rules: No longer build "--with python2".
  * d/README.Debian: Debian's virtualenv package now supports Python 3, so
    drop misleading info that says otherwise.
  * Add 0004-Use-Python-3-everywhere.patch; Fixes ftbfs caused by the py2 drop.

  [ Jan Dittberner ]
  * New upstream version (Closes: #729306, #805877)
  * fix previous changelog entry's bug number
  * refresh patches
  * Install wrapper scripts via dh_install
  * Adapt debian/watch to new pypi.org
  * remove unusable upstream signing key
  * use macros from /usr/share/dpkg/pkg-info.mk instead of parsing dpkg-
    parsechangelog output
  * Update debian/control and debian/copyright
  * d/control: run wrap-and-sort
  * Switch to debhelper 12 compatibility
  * d/control: bump Standards-Version to 4.4.0 (no changes)
  * Refresh patches, drop d/patches/fix-python3-sphinx-build.patch
  * update d/patches/0004-Use-Python-3-everywhere.patch to use Python 3
    in virtualenvwrapper.sh

 -- Jan Dittberner <jandd@debian.org>  Tue, 20 Aug 2019 17:26:21 +0200

virtualenvwrapper (4.3.1-2) unstable; urgency=medium

  * fix bashism in debian/virtualenvwrapper.bash-completion (Closes:
    #763743)

 -- Jan Dittberner <jandd@debian.org>  Sat, 04 Oct 2014 21:01:53 +0200

virtualenvwrapper (4.3.1-1) unstable; urgency=medium

  * New upstream version (Closes: #729507)

 -- Jan Dittberner <jandd@debian.org>  Fri, 26 Sep 2014 16:35:02 +0200

virtualenvwrapper (4.3-1) unstable; urgency=medium

  * New upstream version.
  * upstream changed permissions of created files to safer defaults (Closes:
    #745580)
  * debian/rules: use new upstream ChangeLog
  * bump Standards-Version to 3.9.5 (no changes)
  * Fix "Update Depends for virtualenv" by changing Depends from
    python-virtualenv to virtualenv (Closes: #751191)
  * refresh patches
  * add debian/upstream/signing-key.asc and add pgpsigurlmangle option
    to debian/watch

 -- Jan Dittberner <jandd@debian.org>  Sun, 15 Jun 2014 20:56:38 +0200

virtualenvwrapper (4.1.1-1) unstable; urgency=low

  * New upstream version
  * refresh patches
  * drop debian/patches/debianspecific-setup.py.patch
  * remove files related to multilingual documentation dropped upstream
  * debian/control:
    - add build dependency to python-pbr/python3-bpr
    - use canonical VCS-* URLs
    - bump Standards-Version to 3.9.4 (no changes)
    - add bash-completion to Build-Depends-Indep
    - move bash-completion from Depends to Recommends
  * debian/rules:
    - add PBR_VERSION and SKIP_PIP_INSTALL for pbr in setup.py
    - update sphinx doc source path
    - upstream dropped es and jp translations, same here
    - move virtualenvwrapper.sh and virtualenvwrapper_lazy.sh into
      /usr/share/virtualenvwrapper
    - add a call to dh_bash-completion
  * remove debian/virtualenvwrapper.examples
  * add debian/patches/remove-virtualenvwrapper_lazy-shebang.patch to
    remove shebang and fix path to virtualenvwrapper.sh
  * add debian/virtualenvwrapper.bash-completion with a new way to run
    the bash completion, it defaults to virtualenvwrapper_lazy.sh
    (Closes: #687808, #728373)
  * update information in debian/README.Debian
  * add debian/pydist-overrides to assist dh_python2

 -- Jan Dittberner <jandd@debian.org>  Sun, 03 Nov 2013 10:54:11 +0100

virtualenvwrapper (3.4-2) unstable; urgency=low

  * Fix "FTBFS with python3-sphinx: TypeError: expecting str data, not
    bytes" with debian/patches/fix-python3-sphinx-build.patch (Closes:
    #680863)
  * fix FTBFS twice in a row by removing *.egg-info in debian/rules
    clean

 -- Jan Dittberner <jandd@debian.org>  Mon, 09 Jul 2012 21:25:06 +0200

virtualenvwrapper (3.4-1) unstable; urgency=low

  * New upstream version
  * install virtualenvwrapper_lazy.sh in
    /usr/share/doc/virtualenvwrapper/examples
  * refresh debian/patches/debianspecific-setup.py.patch

 -- Jan Dittberner <jandd@debian.org>  Mon, 21 May 2012 23:11:48 +0200

virtualenvwrapper (3.3-1) unstable; urgency=low

  * New upstream version
  * change Build-Depends for dh_sphinxdoc to python-sphinx (>=
    1.0.7+dfsg) | python3-sphinx
  * bump Standards-Version to 3.9.3 (no changes)
  * refresh debian/patches/debianspecific-setup.py.patch
  * use symlinks for duplicated ajax-loader.gif
  * Fix "FTBFS if built twice in a row: virtualenvwrapper/__init__.pyc:
    binary file contents changed" by removing *.pyc in clean target
    (Closes: #641735)

 -- Jan Dittberner <jandd@debian.org>  Thu, 17 May 2012 14:24:08 +0200

virtualenvwrapper (2.11.1-2) unstable; urgency=low

  * (Closes: #651359: virtualenvwrapper: FTBFS with Sphinx 1.1.2: unknown
    JavaScript code!!) thanks for the patch by Jakub Wilk
    - debian/rules override dh_sphinxdoc invocation
    - debian/control drop dependency on libjs-sphinxdoc

 -- Jan Dittberner <jandd@debian.org>  Sat, 04 Feb 2012 16:04:42 +0100

virtualenvwrapper (2.11.1-1) unstable; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch and
    debian/patches/remove_bitbucket_support.patch
  * update debian/copyright format and copyright years

 -- Jan Dittberner <jandd@debian.org>  Sun, 08 Jan 2012 18:27:55 +0100

virtualenvwrapper (2.8-1) unstable; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch
  * debian/control: get rid of Build-Depends-Indep
  * switch to dh_python2 and dh_sphinxdoc
    - debian/control:
       - remove Build-Depends python-support
       - bump Build-Depends python to 2.6.6-3~
       - bump Build-Depends python-spinx to 1.0.7+dfsg-1~
       - add ${sphinxdoc:Depends} and libjs-sphinxdoc to Depends
       - remove libjs-jquery from Depends
    - debian/rules:
       - add --with python2,sphinxdoc in debian/rules
       - don't dh_link jquery.js
       - don't remove _sources, .doctrees and jquery.js manually
  * add japanese sphinx documentation
    - debian/rules: add ja to lang for loop
    - add debian/virtualenvwrapper.doc-base.ja
    - add /usr/share/doc/virtualenvwrapper/ja to debian/virtualenvwrapper.dirs
  * debian/rules:
    - replace copies of sphinx's _static directory in ja and es with symlinks
      to en
    - simplify sphinx build and set language
  * use upstream sphinx configuration and remove bitbucket usage via
    debian/patches/remove_bitbucket_support.patch
  * don't compress .txt files, to enable sphinx doc search

 -- Jan Dittberner <jandd@debian.org>  Sun, 14 Aug 2011 19:54:16 +0200

virtualenvwrapper (2.7.1-1) unstable; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch
  * debian/control: Bump Standards-Version to 3.9.2

 -- Jan Dittberner <jandd@debian.org>  Sat, 30 Apr 2011 23:24:40 +0200

virtualenvwrapper (2.6.3-1) unstable; urgency=low

  * New upstream version.
  * change debian/watch to look at PyPI
  * refresh debian/patches/debianspecific-setup.py.patch

 -- Jan Dittberner <jandd@debian.org>  Thu, 03 Mar 2011 22:06:55 +0100

virtualenvwrapper (2.6.1-2) unstable; urgency=low

  * upload to unstable

 -- Jan Dittberner <jandd@debian.org>  Sun, 06 Feb 2011 15:28:17 +0100

virtualenvwrapper (2.6.1-1) experimental; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch

 -- Jan Dittberner <jandd@debian.org>  Tue, 28 Dec 2010 10:47:36 +0100

virtualenvwrapper (2.6-1) experimental; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch

 -- Jan Dittberner <jandd@debian.org>  Mon, 27 Dec 2010 15:45:59 +0100

virtualenvwrapper (2.5.2-1) experimental; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch

 -- Jan Dittberner <jandd@debian.org>  Thu, 23 Sep 2010 19:34:15 +0200

virtualenvwrapper (2.5.1-1) experimental; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch

 -- Jan Dittberner <jandd@debian.org>  Tue, 14 Sep 2010 16:18:06 +0200

virtualenvwrapper (2.4-1) experimental; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch

 -- Jan Dittberner <jandd@debian.org>  Thu, 02 Sep 2010 09:42:18 +0200

virtualenvwrapper (2.3-1) experimental; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch
  * debian/control: bump Standards-Version to 3.9.1 (no changes needed)

 -- Jan Dittberner <jandd@debian.org>  Mon, 16 Aug 2010 17:40:29 +0200

virtualenvwrapper (2.2.2-2) unstable; urgency=low

  * add debian/README.Debian to document how virtualenvwrapper is integrated
    with bash-completion
  * add README.Debian to debian/virtualenvwrapper.docs

 -- Jan Dittberner <jandd@debian.org>  Thu, 08 Jul 2010 18:59:54 +0200

virtualenvwrapper (2.2.2-1) unstable; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch
  * debian/control: bump Standards-Version to 3.9.0 (no changes needed)

 -- Jan Dittberner <jandd@debian.org>  Wed, 30 Jun 2010 16:10:31 +0200

virtualenvwrapper (2.2.1-1) unstable; urgency=low

  * New upstream version.
  * remove unnecessary dversionmangle from debian/watch
  * refresh debian/patches/debianspecific-setup.py.patch

 -- Jan Dittberner <jandd@debian.org>  Wed, 23 Jun 2010 21:03:18 +0200

virtualenvwrapper (2.2-1) unstable; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch

 -- Jan Dittberner <jandd@debian.org>  Sat, 29 May 2010 15:13:26 +0200

virtualenvwrapper (2.1.1-1) unstable; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch
  * fix documentation source paths in debian/rules

 -- Jan Dittberner <jandd@debian.org>  Fri, 30 Apr 2010 21:37:05 +0200

virtualenvwrapper (2.1-1) unstable; urgency=low

  * New upstream version.
  * refresh debian/patches/debianspecific-setup.py.patch
  * add README.txt as document in debian/virtualenvwrapper.docs

 -- Jan Dittberner <jandd@debian.org>  Tue, 20 Apr 2010 20:29:16 +0200

virtualenvwrapper (2.0.2-1) unstable; urgency=low

  * New upstream version.
  * remove debian/README.source and get-orig-source target from
    debian/rules because upstream does not bundle sourceless paver-
    minilib.zip anymore
  * modify paths for docs and the script itself in debian/rules to
    new upstream directory structure
  * add python-support and python-setuptools to Build-Depends-Indep and
    add ${python:Depends} to Depends to include upstream's new plugin
    system
  * remove debian/virtualenvwrapper.docs because README is not included
    anymore
  * add debian/patches/debianspecific-setup.py.patch and
    debian/patches/series to adapt to Debian's use of setuptools

 -- Jan Dittberner <jandd@debian.org>  Fri, 09 Apr 2010 15:00:34 +0200

virtualenvwrapper (1.27+dfsg1-1) unstable; urgency=low

  * New upstream version

 -- Jan Dittberner <jandd@debian.org>  Sun, 04 Apr 2010 15:01:57 +0200

virtualenvwrapper (1.26+dfsg1-1) unstable; urgency=low

  * New upstream version (Closes: #574586)
  * remove README.html from debian/virtualenvwrapper.docs it has been
    removed upstream
  * switch to source format 3.0 (quilt)

 -- Jan Dittberner <jandd@debian.org>  Sat, 27 Mar 2010 21:43:41 +0100

virtualenvwrapper (1.24.2+dfsg1-1) unstable; urgency=low

  * Initial release. (Closes: #570644)

 -- Jan Dittberner <jandd@debian.org>  Wed, 24 Feb 2010 22:11:28 +0100
